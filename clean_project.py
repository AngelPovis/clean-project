#!/usr/bin/python3.6

#Written by Neah

import json
import subprocess
import os
import sys
from shutil import make_archive


def open_json(file):
    with open(file) as json_file:
        data = json.load(json_file)
    return data 

def clean_project(json_file):    
        subprocess.call(["rm","-rf", repo['name']])
        subprocess.call(["git","clone" , repo['repository']])
        os.chdir(repo['name'])
        subprocess.call(["git", "checkout" , repo['branch']])
        if len(repo['build'])>0:
            subprocess.call(repo['build'])
        file_delete = repo['ignore']['files']
        dir_delete = repo['ignore']['directories']
        os.putenv('file_delete', ' '.join(file_delete))
        os.putenv('dir_delete', ' '.join(dir_delete))
        if os.name == "nt":
            subprocess.call(["C:\\Program Files\\Git\\git-bash.exe","../../clean_project.sh"])
        else:
            subprocess.call(["../../clean_project.sh"])
        os.chdir("..")
        #subprocess.call(["mv",repo['name'], "repositories" ])
        

json_file = open_json('repositories.json')
# remove repositories directory if exist
subprocess.call(["rm","-rf", "repositories" ])
# create repositories directory
subprocess.call(["mkdir","repositories" ])
os.chdir("repositories")

for repo in json_file:
    clean_project(repo)
os.chdir("..")
make_archive(
  'giru-repositories', 
  'zip',           # the archive format - or tar, bztar, gztar 
  root_dir='repositories',   # root for archive - current working dir if None
  base_dir=None)   # start archiving from here - cwd if None too
