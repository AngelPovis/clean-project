# Clean Projects

## Requisitos
- python
- gitbash

## Configuracion
Objeto JSON de los proyectos a limpiar
```
{
	"name": "project-name",
	"repository": "url-repository",
	"branch": "branch",
	"ignore": {
		"files": ["file1", "file2"],
		"directories" : ["directory1", "directory2"]
	}
	"build": ["build-command"]
}
```
**name**
nombre del repositorio

**repository**
url del repositorio puede ser https o ssh

**branch**
rama en la cual se realizara la limpieza

**ignore.files**
Archivos que seran borrados

**ignore.directories**
Directorios que seran borrados

[doc][https://www.gnu.org/software/findutils/manual/html_node/find_html/Shell-Pattern-Matching.html]

**build**
opcional, es el comando del build del proyecto
`["mvn", "clean", "install"]`

## Ejecucion
**Linux**
`$ python3 clean_project.py` 

Windows
**cmd**
`$ python clean_project.py`
